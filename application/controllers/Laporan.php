<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ------------------------------------------------------------------------
 * CLASS NAME : Laporan
 * ------------------------------------------------------------------------
 *
 * @author     Muhammad Akbar <muslim.politekniktelkom@gmail.com>
 * @copyright  2016
 * @license    http://aplikasiphp.net
 *
 */

class Laporan extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$level 		= $this->session->userdata('ap_level');
		$allowed	= array('admin', 'keuangan');

		if( ! in_array($level, $allowed))
		{
			redirect();
		}
		$this->load->model('m_laporan');
	}

	public function index()
	{
		$data['data'] = $this->m_laporan->fetch_data_tps_all();
		$this->load->view('laporan/form_laporan', $data);
	}

	public function relawan_json()
	{
		$from = $this->input->get('from');
		$to = $this->input->get('to');
		$tps = $this->input->get('tps');

		$level 			= $this->session->userdata('ap_level');

		$requestData	= $_REQUEST;
		$fetch			= $this->m_laporan->fetch_data_laporan_relawan($from, $to, $tps);

		$totalData		= $fetch['totalData'];
		$totalFiltered	= $fetch['totalFiltered'];
		$query			= $fetch['query'];

		$data	= array();
		foreach($query->result_array() as $row)
		{
			if ( $row['status'] == 0 ) $status = 'Aktif'; else $status = 'Tdk Aktif';
			$nestedData = array();
			$nestedData[]	= $row['nomor'];
			$nestedData[]	= $row['kode'];
			$nestedData[]	= $row['nik'];
			$nestedData[]	= $row['nama'];
			$nestedData[]	= preg_replace("/\r\n|\r|\n/",'<br />', $row['alamat']);
			$nestedData[]	= $row['tempat_lahir'];
			$nestedData[]	= $row['tanggal_lahir'];
			$nestedData[]	= $row['jk'];
			$nestedData[]	= $row['status_perkawinan'];
			$nestedData[]	= $row['email'];
			$nestedData[]	= $row['tlp'];
			$nestedData[]	= $row['ref'];
			$nestedData[]	= $row['no_tps'];
			$nestedData[]	= $status;
			if($level == 'admin' OR $level == 'kasir' OR $level == 'keuangan')
			{
				$nestedData[] = '<a class="btn btn-sm btn-info" id=EditRelawan href="'.site_url('relawan/edit/'.$row['id']).'" title="Edit"><i class="fa fa-pencil"></i></a>';
			}

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalData ),
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data
			);

		echo json_encode($json_data);
	}

	public function relawan()
	{
		if ($this->input->post('submit')=='laporan') {
			$data['from'] = $this->input->post('from');
			$data['to'] = $this->input->post('to');
			$data['tps'] = $this->input->post('tps');

			$this->load->view('laporan/laporan_setoran',$data);
		}else{
			$data['from'] = $this->input->post('from');
			$data['to'] = $this->input->post('to');
			$data['tps'] = $this->input->post('tps');

			$temp_tps = array();
			$tps = $this->m_laporan->fetch_data_tps_grafik();
			foreach ($tps as $key) {
				$temp_tps[] = 'TPS-'.$key->no_tps;
			}
			$data['dapil'] = $temp_tps;
			$data['jumlah'] = $this->m_laporan->fetch_data_relawan_per_dapil($data['from'], $data['to']);

			$this->load->view('laporan/laporan_grafik',$data);
		}
	}

	public function excel($from, $to, $sopir)
	{
		$this->load->model('m_setoran');
		$penjualan 	= $this->m_setoran->laporan_setoran($from, $to, $sopir);
		if($penjualan->num_rows() > 0)
		{
			$filename = 'Laporan_Setoran_'.$from.'_'.$to;
			if($sopir != '')
				$filename.='_'.$sopir;
			header("Content-type: application/x-msdownload");
			header("Content-Disposition: attachment; filename=".$filename.".xls");

			echo "
				<h4>Laporan Setoran Tanggal ".date('d/m/Y', strtotime($from))." - ".date('d/m/Y', strtotime($to))."</h4>
				<table border='1' width='100%'>
					<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>No. KTP</th>
							<th>Kode Mobil</th>
							<th>Plat Nomor</th>
							<th>Tanggal</th>
							<th>Setoran</th>
						</tr>
					</thead>
					<tbody>
			";

			$no = 1;
			$total_penjualan = 0;
			foreach($penjualan->result() as $p)
			{
				echo "
					<tr>
						<td>".$no."</td>
						<td>".$p->nama_pelanggan."</td>
						<td>".$p->no_ktp."</td>
						<td>".$p->kode_mobil."</td>
						<td>".$p->plat_nomor."</td>
						<td>".date('d F Y', strtotime($p->tanggal))."</td>
						<td>Rp. ".str_replace(",", ".", number_format($p->grand_total))."</td>
					</tr>
				";

				$total_penjualan += $p->grand_total;
				$no++;
			}

			echo "
				<tr>
					<td colspan='6'><b>Total Setoran</b></td>
					<td><b>Rp. ".str_replace(",", ".", number_format($total_penjualan))."</b></td>
				</tr>
			</tbody>
			</table>
			";
		}
	}

	public function pdf($from, $to, $sopir)
	{
		$this->load->library('cfpdf');

		$pdf = new FPDF();
		$pdf->AddPage('L');
		$pdf->SetFont('Arial','B',10);

		$pdf->SetFont('Arial','',10);
		$pdf->Cell(0, 8, "Laporan Setoran Tanggal ".date('d/m/Y', strtotime($from))." - ".date('d/m/Y', strtotime($to)), 0, 1, 'L');

		$pdf->Cell(15, 7, 'No', 1, 0, 'L');
		$pdf->Cell(50, 7, 'Nama', 1, 0, 'L');
		$pdf->Cell(60, 7, 'No. KTP', 1, 0, 'L');
		$pdf->Cell(20, 7, 'Kode Mobil', 1, 0, 'L');
		$pdf->Cell(20, 7, 'Plat Nomor', 1, 0, 'L');
		$pdf->Cell(50, 7, 'Tanggal', 1, 0, 'L');
		$pdf->Cell(50, 7, 'Setoran', 1, 0, 'L');
		$pdf->Ln();

		$this->load->model('m_setoran');
		$penjualan 	= $this->m_setoran->laporan_setoran($from, $to, $sopir);

		$no = 1;
		$total_penjualan = 0;
		foreach($penjualan->result() as $p)
		{
			$pdf->Cell(15, 7, $no, 1, 0, 'L');
			$pdf->Cell(50, 7, $p->nama_pelanggan, 1, 0, 'L');
			$pdf->Cell(60, 7, $p->no_ktp, 1, 0, 'L');
			$pdf->Cell(20, 7, $p->kode_mobil, 1, 0, 'L');
			$pdf->Cell(20, 7, $p->plat_nomor, 1, 0, 'L');
			$pdf->Cell(50, 7, $p->tanggal, 1, 0, 'L');
			$pdf->Cell(50, 7, "Rp. ".str_replace(",", ".", number_format($p->grand_total)), 1, 0, 'L');
			$pdf->Ln();

			$total_penjualan += $p->grand_total;
			$no++;
		}

		$pdf->Cell(215, 7, 'Total Setoran', 1, 0, 'L');
		$pdf->Cell(50, 7, "Rp. ".str_replace(",", ".", number_format($total_penjualan)), 1, 0, 'L');
		$pdf->Ln();

		$pdf->Output();
	}
}
