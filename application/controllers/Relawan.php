<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ------------------------------------------------------------------------
 * CLASS NAME : User
 * ------------------------------------------------------------------------
 *
 * @author     Muhammad Akbar <muslim.politekniktelkom@gmail.com>
 * @copyright  2016
 * @license    http://aplikasiphp.net
 *
 */

class Relawan extends MY_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('m_relawan');
		$this->load->model('m_user');
    }

	public function index()
	{
		$level = $this->session->userdata('ap_level');
		if($level !== 'admin')
		{
			exit();
		}
		else
		{
			$this->load->view('relawan/relawan_data');
		}
	}

	public function relawan_json()
	{
		$level 			= $this->session->userdata('ap_level');

		$requestData	= $_REQUEST;
		$fetch			= $this->m_relawan->fetch_data_relawan($requestData['search']['value'], $requestData['start'], $requestData['length']);

		$totalData		= $fetch['totalData'];
		$totalFiltered	= $fetch['totalFiltered'];
		$query			= $fetch['query'];

		$data	= array();
		foreach($query->result_array() as $row)
		{
			if ( $row['status'] == 0 ) $status = 'Aktif'; else $status = 'Tdk Aktif';
			$nestedData = array();
			$nestedData[]	= $row['nomor'];
			$nestedData[]	= $row['kode'];
			$nestedData[]	= $row['nik'];
			$nestedData[]	= $row['nama'];
			$nestedData[]	= preg_replace("/\r\n|\r|\n/",'<br />', $row['alamat']);
			$nestedData[]	= $row['tempat_lahir'];
			$nestedData[]	= $row['tanggal_lahir'];
			$nestedData[]	= $row['jk'];
			$nestedData[]	= $row['status_perkawinan'];
			$nestedData[]	= $row['email'];
			$nestedData[]	= $row['tlp'];
			$nestedData[]	= $row['ref'];
			$nestedData[]	= $row['no_tps'];
			$nestedData[]	= $status;
			if($level == 'admin' OR $level == 'kasir' OR $level == 'keuangan')
			{
				$nestedData[] = '<a class="btn btn-sm btn-danger" id=EditRelawan href="'.site_url('relawan/edit/'.$row['id']).'" title="Edit"><i class="fa fa-pencil"></i></a>';
			}

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalData ),
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data
			);

		echo json_encode($json_data);
	}

	public function hapus($id_relawan)
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin')
		{
			if($this->input->is_ajax_request())
			{
				$this->load->model('m_relawan');
				$hapus = $this->m_relawan->hapus_relawan($id_relawan);
				if($hapus)
				{
					echo json_encode(array(
						"pesan" => "<font color='green'><i class='fa fa-check'></i>Disable driver berhasil !</font>
					"));
				}
				else
				{
					echo json_encode(array(
						"pesan" => "<font color='red'><i class='fa fa-warning'></i> Terjadi kesalahan, coba lagi !</font>
					"));
				}
			}
		}
	}

	public function enable($id_relawan)
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin')
		{
			if($this->input->is_ajax_request())
			{
				$this->load->model('m_relawan');
				$hapus = $this->m_relawan->enable_relawan($id_relawan);
				if($hapus)
				{
					echo json_encode(array(
						"pesan" => "<font color='green'><i class='fa fa-check'></i> Enable driver berhasil !</font>
					"));
				}
				else
				{
					echo json_encode(array(
						"pesan" => "<font color='red'><i class='fa fa-warning'></i> Terjadi kesalahan, coba lagi !</font>
					"));
				}
			}
		}
	}

	public function tambah()
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'kasir' OR $level == 'keuangan')
		{
			if($_POST)
			{
				$this->load->library('form_validation');
				$this->form_validation->set_rules('nik','NIK','trim|required|numeric|max_length[40]');
				$this->form_validation->set_rules('nama','Nama','trim|required|alpha_spaces|max_length[40]');
				$this->form_validation->set_rules('alamat','Alamat','trim|required|max_length[1000]');
				$this->form_validation->set_rules('tempat_lahir','Tempat Lahir','trim|required|max_length[100]');
				$this->form_validation->set_rules('tanggal_lahir','Tanggal Lahir','trim|required|max_length[40]');
				$this->form_validation->set_rules('jk','Jenis Kelamin','trim|required|max_length[40]');
				$this->form_validation->set_rules('status_perkawinan','Status Perkawinan','trim|required|max_length[40]');
				$this->form_validation->set_rules('tlp','Telp','trim|required|numeric|max_length[40]');
				$this->form_validation->set_rules('email','Email','trim|max_length[100]');

				$this->form_validation->set_message('alpha_spaces','%s harus alphabet !');
				$this->form_validation->set_message('numeric','%s harus angka !');
				$this->form_validation->set_message('required','%s harus diisi !');

				if($this->form_validation->run() == TRUE)
				{
					$kode = $this->input->post('kode');
	                $nama = $this->input->post('nama');
	                $nik = $this->input->post('nik');
	                $alamat = $this->clean_tag_input($this->input->post('alamat'));
	                $tanggal_lahir = $this->input->post('tanggal_lahir');
	                $tempat_lahir = $this->input->post('tempat_lahir');
	                $jk = $this->input->post('jk');
	                $status_perkawinan = $this->input->post('status_perkawinan');
	                $email = strtolower($this->input->post('email'));
	                $tlp = $this->input->post('tlp');
					$id_tps = $this->input->post('tps');
					$id_ref = $this->input->post('ref');

					$userInfo = array('kode'=>$kode, 'nama'=>$nama, 'nik'=> $nik, 'alamat' => $alamat,
										'tanggal_lahir' => $tanggal_lahir, 'tempat_lahir' => $tempat_lahir,
										'jk'=> $jk, 'status_perkawinan'=>$status_perkawinan, 'email'=>$email,
										'tlp' => $tlp, 'id_tps' => $id_tps, 'id_ref'=> $id_ref,
										'created_by'=>$this->session->userdata('ap_id_user'), 'created_at'=>date('Y-m-d H:i:s'));

					$insert = $this->m_relawan->tambah_relawan($userInfo);

					if($insert)
					{
						$id_relawan = $this->m_relawan->get_dari_kode($nik)->row()->id;
						echo json_encode(array(
							'status' => 1,
							'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> <b>".$nama."</b> berhasil ditambahkan sebagai relawan.</div>",
							'id_relawan' => $id_relawan,
							'nik' => $nik,
							'nama' => $nama,
							'alamat' => preg_replace("/\r\n|\r|\n/",'<br />', $alamat),
							'tlp' => $tlp
						));
					}
					else
					{
						$this->query_error(json_encode($userInfo));
					}
				}
				else
				{
					$this->input_error();
				}
			}
			else
			{
				$kode['kode'] = $this->m_relawan->get_kode();
				$this->load->view('relawan/relawan_tambah', $kode);
			}
		}
	}

	public function exist_username($username)
	{

		$cek_user = $this->m_user->cek_username($username);

		if($cek_user->num_rows() > 0)
		{
			return FALSE;
		}
		return TRUE;
	}

	public function edit($id_relawan = NULL)
	{
		if( ! empty($id_relawan))
		{
			$level = $this->session->userdata('ap_level');
			if($level == 'admin' OR $level == 'kasir' OR $level == 'keuangan')
			{
				if($this->input->is_ajax_request())
				{
					$this->load->model('m_relawan');

					if($_POST)
					{
						$this->load->library('form_validation');
						$this->form_validation->set_rules('nik','NIK','trim|required|numeric|max_length[40]');
						$this->form_validation->set_rules('nama','Nama','trim|required|alpha_spaces|max_length[40]');
						$this->form_validation->set_rules('alamat','Alamat','trim|required|max_length[1000]');
						$this->form_validation->set_rules('tempat_lahir','Tempat Lahir','trim|required|max_length[100]');
						$this->form_validation->set_rules('tanggal_lahir','Tanggal Lahir','trim|required|max_length[40]');
						$this->form_validation->set_rules('jk','Jenis Kelamin','trim|required|max_length[40]');
						$this->form_validation->set_rules('status_perkawinan','Status Perkawinan','trim|required|max_length[40]');
						$this->form_validation->set_rules('tlp','Telp','trim|required|numeric|max_length[40]');
						$this->form_validation->set_rules('email','Email','trim|max_length[100]');

						$this->form_validation->set_message('alpha_spaces','%s harus alphabet !');
						$this->form_validation->set_message('numeric','%s harus angka !');
						$this->form_validation->set_message('required','%s harus diisi !');

						if($this->form_validation->run() == TRUE)
						{
							$kode = $this->input->post('kode');
			                $nama = $this->input->post('nama');
			                $nik = $this->input->post('nik');
			                $alamat = $this->clean_tag_input($this->input->post('alamat'));
			                $tanggal_lahir = $this->input->post('tanggal_lahir');
			                $tempat_lahir = $this->input->post('tempat_lahir');
			                $jk = $this->input->post('jk');
			                $status_perkawinan = $this->input->post('status_perkawinan');
			                $email = strtolower($this->input->post('email'));
			                $tlp = $this->input->post('tlp');
							$id_tps = $this->input->post('tps');
							$id_ref = $this->input->post('ref');
							$status = $this->input->post('status');

							if (empty($id_ref) && empty($id_tps))
							$userInfo = array('kode'=>$kode, 'nama'=>$nama, 'nik'=> $nik, 'alamat' => $alamat,
												'tanggal_lahir' => $tanggal_lahir, 'tempat_lahir' => $tempat_lahir,
												'jk'=> $jk, 'status_perkawinan'=>$status_perkawinan, 'email'=>$email,
												'tlp' => $tlp, 'created_by'=>$this->session->userdata('ap_id_user'),
												'status'=>$status, 'created_at'=>date('Y-m-d H:i:s'));
							else if(empty($id_tps))
							$userInfo = array('kode'=>$kode, 'nama'=>$nama, 'nik'=> $nik, 'alamat' => $alamat,
												'tanggal_lahir' => $tanggal_lahir, 'tempat_lahir' => $tempat_lahir,
												'jk'=> $jk, 'status_perkawinan'=>$status_perkawinan, 'email'=>$email,
												'tlp' => $tlp, 'id_ref'=> $id_ref, 'created_by'=>$this->session->userdata('ap_id_user'),
												'status'=>$status, 'created_at'=>date('Y-m-d H:i:s'));
							else if(empty($id_ref))
							$userInfo = array('kode'=>$kode, 'nama'=>$nama, 'nik'=> $nik, 'alamat' => $alamat,
												'tanggal_lahir' => $tanggal_lahir, 'tempat_lahir' => $tempat_lahir,
												'jk'=> $jk, 'status_perkawinan'=>$status_perkawinan, 'email'=>$email,
												'tlp' => $tlp, 'id_tps' => $id_tps, 'created_by'=>$this->session->userdata('ap_id_user'),
												'status'=>$status, 'created_at'=>date('Y-m-d H:i:s'));
							else
							$userInfo = array('kode'=>$kode, 'nama'=>$nama, 'nik'=> $nik, 'alamat' => $alamat,
												'tanggal_lahir' => $tanggal_lahir, 'tempat_lahir' => $tempat_lahir,
												'jk'=> $jk, 'status_perkawinan'=>$status_perkawinan, 'email'=>$email,
												'tlp' => $tlp, 'id_tps' => $id_tps, 'id_ref'=> $id_ref, 'status'=>$status,
												'created_by'=>$this->session->userdata('ap_id_user'), 'created_at'=>date('Y-m-d H:i:s'));

							$update 	= $this->m_relawan->update_relawan($id_relawan, $userInfo);
							if($update)
							{
								echo json_encode(array(
									'status' => 1,
									'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> Data berhasil diupdate.</div>"
								));
							}
							else
							{
								$this->query_error();
							}
						}
						else
						{
							$this->input_error();
						}
					}
					else
					{
						$dt['relawan'] = $this->m_relawan->get_baris($id_relawan)->row();
						$this->load->view('relawan/relawan_edit', $dt);
					}
				}
			}
		}
	}

	public function ubah_password()
	{
		if($this->input->is_ajax_request())
		{
			if($_POST)
			{
				$this->load->library('form_validation');
				$this->form_validation->set_rules('pass_old','Password Lama','trim|required|max_length[60]|callback_check_pass[pass_old]');
				$this->form_validation->set_rules('pass_new','Password Baru','trim|required|max_length[60]');
				$this->form_validation->set_rules('pass_new_confirm','Ulangi Password Baru','trim|required|max_length[60]|matches[pass_new]');
				$this->form_validation->set_message('required','%s harus diisi !');
				$this->form_validation->set_message('check_pass','%s anda salah !');

				if($this->form_validation->run() == TRUE)
				{
					$this->load->model('m_user');
					$pass_new 	= $this->input->post('pass_new');

					$update 	= $this->m_user->update_password($pass_new);
					if($update)
					{
						$this->session->set_userdata('ap_password', sha1($pass_new));

						echo json_encode(array(
							'status' => 1,
							'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> Password berhasil diupdate.</div>"
						));
					}
					else
					{
						$this->query_error();
					}
				}
				else
				{
					$this->input_error();
				}
			}
			else
			{
				$this->load->view('user/change_pass');
			}
		}
	}

	public function check_pass($pass)
	{
		$cek_user = $this->m_user->cek_password($pass);

		if($cek_user->num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}

	public function get_tps()
	{
		echo json_encode($this->m_relawan->fetch_data_tps($this->input->get('q')));
	}

	public function get_ref()
	{
		echo json_encode($this->m_relawan->fetch_data_ref_edit($this->input->get('q'), $this->input->get('id')));
	}
}
