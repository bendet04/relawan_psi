<?php
class M_Laporan extends CI_Model
{
	function get_all()
	{
		return $this->db
			->select('*')
			->where('status', '0')
			->order_by('nama','asc')
			->get('tbl_relawan');
	}

	function get_baris($id)
	{
		return $this->db
			->select('*')
			->where('id', $id)
			->limit(1)
			->get('tbl_relawan');
	}

	function fetch_data_laporan_relawan($from, $to, $tps)
	{
		$sql = "
			SELECT
				(@row:=@row+1) AS nomor,
				a.`id`,
				a.`kode`,
				a.`nik`,
				a.`nama`,
				a.`alamat`,
				a.`tempat_lahir`,
				a.`tanggal_lahir`,
				a.`jk`,
				a.`status_perkawinan`,
				a.`tlp`,
				a.`email`,
				a.`status`,
				a.`created_at`,
				if (a.`id_ref` != '', (select nama from tbl_relawan where id = a.`id_ref`), 'null' ) as ref,
				b.`no_tps`

			FROM
				`tbl_relawan` AS a
				LEFT JOIN `tbl_tps` AS b ON a.`id_tps` = b.`id`
				, (SELECT @row := 0) r WHERE 1=1

		";

		$sql .=" AND DATE_FORMAT(a.`created_at`,'%Y-%m-%d') BETWEEN '".$from."' AND '".$to."' ";
		if( $tps != 0)
		{
			$sql .= " AND ( ";
			$sql .= "
				a.`id_tps` = '".$tps."'";
			$sql .= " ) ";
		}
		$sql .=" AND a.`status` = '0' ";

		$data['totalData'] = $this->db->query($sql)->num_rows();

		$data['totalFiltered']	= $this->db->query($sql)->num_rows();

		$data['query'] = $this->db->query($sql);
		return $data;
	}

	function tambah_relawan($relawan_info)
	{
		return $this->db->insert('tbl_relawan', $relawan_info);
	}

	function update_relawan($id, $userInfo)
	{

		return $this->db
			->where('id', $id)
			->update('tbl_relawan', $userInfo);
	}

	function hapus_relawan($id)
	{
		$dt = array(
			'status' => 'disable'
		);

		return $this->db
			->where('id', $id)
			->update('relawan', $dt);
	}

	function enable_relawan($id)
	{
		$dt = array(
			'status' => 'enable'
		);

		return $this->db
			->where('id', $id)
			->update('relawan', $dt);
	}


	function get_dari_kode($nik)
	{
		return $this->db
			->select('id')
			->where('nik', $nik)
			->limit(1)
			->get('tbl_relawan');
	}

	function cari_kode($keyword, $registered)
	{
		$not_in = '';

		$koma = explode(',', $registered);
		if(count($koma) > 1)
		{
			$not_in .= " AND `no_ktp` NOT IN (";
			foreach($koma as $k)
			{
				$not_in .= " '".$k."', ";
			}
			$not_in = rtrim(trim($not_in), ',');
			$not_in = $not_in.")";
		}
		if(count($koma) == 1)
		{
			$not_in .= " AND `no_ktp` != '".$registered."' ";
		}

		$sql = "
			SELECT
				`id`,`no_ktp`,`nama`,`ks`
			FROM
				`tbl_relawan`
			WHERE
				`status` = 'enable'
				AND (
					`no_ktp` LIKE '%".$this->db->escape_like_str($keyword)."%'
					OR `nama` LIKE '%".$this->db->escape_like_str($keyword)."%'
				)
				".$not_in."
		";

		return $this->db->query($sql);
	}

	public function get_kode(){
        $q = $this->db->query("SELECT MAX(RIGHT(kode, 4)) AS kd_max
							FROM tbl_relawan WHERE DATE(created_at)=CURDATE()");
        $kd = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%04s", $tmp);
            }
        }else{
            $kd = "0001";
        }
        date_default_timezone_set('Asia/Jakarta');
        return date('dmy').$kd;
    }

	function fetch_data_tps_all()
	{
		$this->db->like('no_tps');
		$query = $this->db->select('*')
						->get("tbl_tps");
		return $query->result();
	}

	function fetch_data_tps($data)
	{
		$this->db->like('no_tps', $data);
		$query = $this->db->select('id, no_tps as text')
						->limit(10)
						->get("tbl_tps");
		return $query->result_array();
	}

	function fetch_data_ref($data)
	{
		$this->db->like('nik', $data);
		$query = $this->db->select('id, nik as text')
						->limit(10)
						->get("tbl_relawan");
		return $query->result_array();
	}

	function fetch_data_ref_edit($data, $id)
	{
		$this->db->like('nik', $data);
		$this->db->where('id !=', $id);
		$query = $this->db->select('id, nik as text')
						->limit(10)
						->get("tbl_relawan");
		return $query->result_array();
	}

	function fetch_data_tps_grafik(){
		$query = $this->db->select('no_tps')
						->get("tbl_tps");
		return $query->result();
	}

	function fetch_data_relawan_per_dapil($from, $to){
		$data = array();
		$this->db->select('*');
		$this->db->from('tbl_tps');

		foreach ($this->db->get()->result() as $key) {
			//get jumlah permohonan by program
			$this->db->select('count(id) as jumlah');
			$this->db->from('tbl_relawan');
			$this->db->where('id_tps', $key->id);
			$this->db->where('status', '0' );
			$this->db->where("DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN '".$from."' AND '".$to."' ");

			$jumlah = $this->db->get()->row();
			$data[] = (int) $jumlah->jumlah;
		}
		return $data;
	}

}
