
<link href="<?php echo config_item('bootstrap'); ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo config_item('bootstrap'); ?>css/bootstrap-theme.min.css" rel="stylesheet">
<link href="<?php echo config_item('font_awesome'); ?>css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo config_item('plugin'); ?>datatables/css/dataTables.bootstrap.css"/>
<link href="<?php echo config_item('css'); ?>style-gue.css" rel="stylesheet">
<script src="<?php echo config_item('js'); ?>jquery.min.js"></script>
<title>Laporan</title>
</br>
<div class="container-fluid">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class='table-responsive'>
				<table id="my-grid" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode</th>
							<th>NIK</th>
							<th style="min-width:100px">Nama</th>
							<th>Alamat</th>
							<th style="min-width:100px">Tempat Lahir</th>
							<th style="min-width:120px">Tanggal Lahir</th>
							<th>JK</th>
							<th style="min-width:100px">Status Kawin</th>
							<th>Email</th>
							<th style="min-width:100px">Telp. / HP</th>
							<th>Refrensi</th>
							<th style="min-width:80px">No Tps</th>
							<th style="min-width:80px">Status</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" language="javascript" src="<?php echo config_item('plugin'); ?>datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo config_item('plugin'); ?>datatables/js/dataTables.bootstrap.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

<script type="text/javascript" language="javascript" >

$(document).ready(function() {
	var dataTable = $('#my-grid').DataTable( {
		"serverSide": true,
		"stateSave" : false,
		"scrollX" : true,
		"paging": false,
		"ordering": false,
		"searching": false,
		"oLanguage": {
			"sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
			"sZeroRecords": "Pencarian tidak ditemukan",
			"sEmptyTable": "Data kosong",
			"sLoadingRecords": "Harap Tunggu...",
		},
		"ajax":{
			url :"<?php echo site_url('laporan/relawan-json'); ?>",
			data: {
				"from": "<?php echo $from; ?>",
				"to": "<?php echo $to; ?>",
				"tps": "<?php echo $tps; ?>"
			},
			error: function(){
				$(".my-grid-error").html("");
				$("#my-grid").append('<tbody class="my-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#my-grid_processing").css("display","none");
			}
		},
		"dom": 'Bfrtip',
		"buttons": [
			'copy', 'csv', 'excel', 'pdf', 'print'
		],
		"columnDefs": [
			{ targets: [0], visible: false},
		]
	});
});
</script>
