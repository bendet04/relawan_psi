<script src="<?php echo config_item('js'); ?>jquery.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<section class="section section-blog">
    <div class="container">
        <div class="panel panel-default">
            <div class="card" style="margin-bottom:10px; margin-top:20px">
                <div class="card-body">
                    <div  style="width: 100%; margin-top: 40px;">
                        <div id="canvas"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</section>

<script type="text/javascript">

    $('#canvas').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Report',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: 'Relawan',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($dapil);?>
        },
        exporting: {
            enabled: false
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
        },
        series: [{
            name: 'Dapil',
            data: <?php echo json_encode($jumlah);?>,
            shadow : true
        }]
    });

</script>
