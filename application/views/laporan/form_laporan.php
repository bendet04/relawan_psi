<?php $this->load->view('include/header'); ?>
<?php $this->load->view('include/navbar'); ?>

<?php
$level = $this->session->userdata('ap_level');
?>
<style>
.panel {
  margin-right: auto;
  margin-left: auto;
  max-width: 600px; /* or 950px */
}
</style>

<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			<h5><i class='fa fa-file-text-o fa-fw'></i> Laporan Relawan</h5>
			<hr />
			<form method="post" target="print_popup" action="laporan/relawan"
			onsubmit="window.open('about:blank','print_popup','width=1200,height=500');">
			<div class="row">
				<div class="col-sm-10">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-4 control-label">Dari Tanggal</label>
							<div class="col-sm-8">
								<input type='text' name='from' class='form-control' id='tanggal_dari' value="<?php echo date('Y-m-d'); ?>" autocomplete=off readonly>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-10">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-4 control-label">Sampai Tanggal</label>
							<div class="col-sm-8">
								<input type='text' name='to' class='form-control' id='tanggal_sampai' value="<?php echo date('Y-m-d'); ?>" autocomplete=off readonly>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-10">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-4 control-label">TPS</label>
							<div class="col-sm-8">
								<select name='tps' class='form-control'>
									<option value='0'>Select All TPS</option>
									<?php
									for ($i = 0; $i < count($data); $i++)
									{
										echo "<option value='".$data[$i]->id."'>".$data[$i]->no_tps."</option>";
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='row'>
				<div class="col-sm-10">
					<div class="form-horizontal">
						<div class="form-group">
							<div class="col-sm-4"></div>
							<div class="col-sm-8">
								<button type="submit" class="btn btn-danger" name="submit" value="laporan" style='margin-left: 0px;'>Tampilkan Report</button>
                                <button type="submit" class="btn btn-primary" name="submit" value="grafik" style='margin-left: 0px;'>Tampilkan Chart</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<p class='footer'><?php echo config_item('web_footer'); ?></p>

<link rel="stylesheet" type="text/css" href="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.css"/>
</div>
<script src="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.js"></script>
<script>

$('#tanggal_dari').datetimepicker({
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	closeOnDateSelect:true
});
$('#tanggal_sampai').datetimepicker({
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	closeOnDateSelect:true
});


</script>

<?php $this->load->view('include/footer'); ?>
