<?php echo form_open('relawan/edit/'.$relawan->id, array('id' => 'FormEditrelawan')); ?>

<div class="form-horizontal">
	<div class='form-group'>
		<label class="col-sm-3 control-label">Kode</label>
		<div class="col-sm-8">
			<?php
			echo form_input(array(
				'name' => 'kode',
				'class' => 'form-control',
				'value' => $relawan->kode,
				'readonly'=>'true'
			));
			?>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">NIK</label>
		<div class="col-sm-8">
			<?php
			echo form_input(array(
				'name' => 'nik',
				'class' => 'form-control',
				'value' => $relawan->nik
			));
			?>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Nama</label>
		<div class="col-sm-8">
			<?php
			echo form_input(array(
				'name' => 'nama',
				'class' => 'form-control',
				'value' => $relawan->nama
			));
			?>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Alamat</label>
		<div class="col-sm-8">
			<?php
			echo form_textarea(array(
				'name' => 'alamat',
				'class' => 'form-control',
				'value' => $relawan->alamat,
				'style' => "resize:vertical",
				'rows' => 3
			));
			?>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">TPS</label>
		<div class="col-sm-8">
			<select class="form-control required" id="tps" name="tps"/>
			<p>Kosongkan jika tidak diganti</p>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Refrensi</label>
		<div class="col-sm-8">
			<select class="form-control required" id="ref" name="ref"/>
			<p>Kosongkan jika tidak diganti</p>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Tempat Lahir</label>
		<div class="col-sm-8">
			<?php
			echo form_input(array(
				'name' => 'tempat_lahir',
				'class' => 'form-control',
				'value' => $relawan->tempat_lahir
			));
			?>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Tanggal Lahir</label>
		<div class="col-sm-8">
			<?php
			echo form_input(array(
				'name' => 'tanggal_lahir',
				'id' => 'tanggal_lahir',
				'class' => 'form-control',
				'autocomplete'=>'off',
				'readonly'=>'true',
				'value' => $relawan->tanggal_lahir
			));
			?>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Jenis Kelamin</label>
		<div class="col-sm-8">
			<select name='jk' class='form-control'>
				<option value='0'>Select JK</option>
				<?php
					 $status = array('L', 'P');

						for ($i = 0; $i < 2; $i++)
						{
							$selected = '';
							if($status[$i] == $relawan->jk){
								$selected = 'selected';
							}
							echo "<option value='".$status[$i]."' ".$selected.">".$status[$i]."</option>";
						}
				?>
			</select>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Status Kawin</label>
		<div class="col-sm-8">
			<select class="form-control required" id="status_perkawinan" name="status_perkawinan">
				<option value='0'>Select Status Perkawinan</option>
				<?php
					 $status = array('Kawin', 'Belum Kawin', 'Pernah Kawin');

						for ($i = 0; $i < 3; $i++)
						{
							$selected = '';
							if($status[$i] == $relawan->status_perkawinan){
								$selected = 'selected';
							}
							echo "<option value='".$status[$i]."' ".$selected.">".$status[$i]."</option>";
						}
				?>
			</select>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Telp</label>
		<div class="col-sm-8">
			<?php
			echo form_input(array(
				'name' => 'tlp',
				'class' => 'form-control',
				'value' => $relawan->tlp
			));
			?>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Email</label>
		<div class="col-sm-8">
			<?php
			echo form_input(array(
				'name' => 'email',
				'class' => 'form-control',
				'value' => $relawan->email
			));
			?>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Status</label>
		<div class="col-sm-8">
			<select name='status' class='form-control'>
				<?php
					 $status = array('0', '1');

						for ($i = 0; $i < 2; $i++)
						{
							$selected = '';
							if ($status[$i] == '0') $stt = 'Aktif'; else $stt = 'Tidak Aktif';

							if($status[$i] == $relawan->status){
								$selected = 'selected';
							}
							echo "<option value='".$status[$i]."' ".$selected.">".$stt."</option>";
						}
				?>
			</select>
		</div>
	</div>
</div>

<?php echo form_close(); ?>

<div id='ResponseInput'></div>
<link rel="stylesheet" type="text/css" href="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.css"/>
<script src="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.js"></script>
<script>

$('#ModalGue').on('hidden.bs.modal', function () {
	$("#tanggal_lahir").datetimepicker("hide");
});

function Editrelawan()
{
	$.ajax({
		url: $('#FormEditrelawan').attr('action'),
		type: "POST",
		cache: false,
		data: $('#FormEditrelawan').serialize(),
		dataType:'json',
		success: function(json){
			if(json.status == 1){
				$('#ResponseInput').html(json.pesan);
				setTimeout(function(){
			   		$('#ResponseInput').html('');
			    }, 3000);
				$('#my-grid').DataTable().ajax.reload( null, false );
			}
			else {
				$('#ResponseInput').html(json.pesan);
			}
		}
	});
}

$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-danger' id='SimpanEditrelawan'>Update Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$("#FormEditrelawan").find('input[type=text],textarea,select').filter(':visible:first').focus();

	$('#SimpanEditrelawan').click(function(e){
		e.preventDefault();
		Editrelawan();
	});

	$('#FormEditrelawan').submit(function(e){
		e.preventDefault();
		Editrelawan();
	});

	$('#tanggal_lahir').datetimepicker({
		lang:'en',
		timepicker:false,
		format:'Y-m-d',
		closeOnDateSelect:true
	});

	$( "#tps" ).select2({
   		dropdownParent: $("#ModalGue"),
		placeholder: ' Select No TPS',
		allowClear: true,
		ajax: {
			url: "<?php echo base_url('relawan/get_tps')?>",
			dataType: 'json',
			delay: 0,
			data: function (params) {
				return {
					q: params.term
				};
			},
			processResults: function (data) {
				return {
					results: data
				};
			},

			cache: false
		}
	});

	$( "#ref" ).select2({
   		dropdownParent: $("#ModalGue"),
		placeholder: ' Select Refrensi',
		allowClear: true,
		ajax: {
			url: "<?php echo base_url('relawan/get_ref')?>",
			dataType: 'json',
			delay: 0,
			data: function (params) {
				return {
					q: params.term,
					id: "<?php echo $relawan->id ?>"
				};
			},
			processResults: function (data) {
				// parse the results into the format expected by Select2.
				// since we are using custom formatting functions we do not need to
				// alter the remote JSON data
				return {
					results: data
				};
			},
			cache: false
		}
	});
});
</script>
