
<?php echo form_open('relawan/tambah', array('id' => 'FormTambahPelanggan')); ?>
<div class="form-horizontal">
	<div class='form-group'>
		<label class="col-sm-3 control-label">Kode</label>
		<div class="col-sm-8">
			<input type='text' name='kode' class='form-control' readonly value="<?php echo $kode; ?>">
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">NIK</label>
		<div class="col-sm-8">
			<input type='text' name='nik' class='form-control'>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Nama</label>
		<div class="col-sm-8">
			<input type='text' name='nama' class='form-control'>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Alamat</label>
		<div class="col-sm-8">
			<textarea name='alamat' class='form-control' style='resize:vertical;'></textarea>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">TPS</label>
		<div class="col-sm-8">
			<select class="form-control required" id="tps" name="tps"/>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Refrensi</label>
		<div class="col-sm-8">
			<select class="form-control required" id="ref" name="ref"/>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Tempat Lahir</label>
		<div class="col-sm-8">
			<input type='text' name='tempat_lahir' class='form-control'>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Tanggal Lahir</label>
		<div class="col-sm-8">
			<input type='text' name='tanggal_lahir' id='tanggal_lahir' class='form-control'  autocomplete="off" readonly>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Jenis Kelamin</label>
		<div class="col-sm-8">
			<select class="form-control required" id="jk" name="jk">
				<option value="0">Select JK</option>
				<option value="P" >P</option>
				<option value="L" >L</option>
			</select>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Status Kawin</label>
		<div class="col-sm-8">
			<select class="form-control required" id="status_perkawinan" name="status_perkawinan">
				<option value="0">Select Status</option>
				<option value="Kawin" >Kawin</option>
				<option value="Belum Kawin" >Belum Kawin</option>
				<option value="Pernah Kawin" >Pernah Kawin</option>
			</select>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Telp</label>
		<div class="col-sm-8">
			<input type='number' name='tlp' class='form-control'>
		</div>
	</div>
	<div class='form-group'>
		<label class="col-sm-3 control-label">Email</label>
		<div class="col-sm-8">
			<input type='email' name='email' class='form-control'>
		</div>
	</div>

</div>
<?php echo form_close(); ?>

<div id='ResponseInput'></div>

<link rel="stylesheet" type="text/css" href="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.css"/>
<script src="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.js"></script>
<script>
$('#ModalGue').on('hidden.bs.modal', function () {
	$("#tanggal_lahir").datetimepicker("hide");
});

function TambahPelanggan()
{
	$.ajax({
		url: $('#FormTambahPelanggan').attr('action'),
		type: "POST",
		cache: false,
		data: $('#FormTambahPelanggan').serialize(),
		dataType:'json',
		success: function(json){
			if(json.status == 1)
			{
				$('#FormTambahPelanggan').each(function(){
					this.reset();
				});

				if(document.getElementById('PelangganArea') != null)
				{
					$('#ResponseInput').html('');

					$('.modal-dialog').removeClass('modal-lg');
					$('.modal-dialog').addClass('modal-sm');
					$('#ModalHeader').html('Berhasil');
					$('#ModalContent').html(json.pesan);
					$('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Okay</button>");
					$('#ModalGue').modal('show');

					$('#id_pelanggan').append("<option value='"+json.id_pelanggan+"' selected>"+json.nama+"</option>");
					$('#telp_pelanggan').html(json.telepon);
					$('#alamat_pelanggan').html(json.alamat);
					$('#info_tambahan_pelanggan').html(json.info);
				}
				else
				{
					$('#ResponseInput').html(json.pesan);
					setTimeout(function(){
				   		$('#ResponseInput').html('');
				    }, 3000);
					$('#my-grid').DataTable().ajax.reload( null, false );
				}
			}
			else
			{
				$('#ResponseInput').html(json.pesan);
			}
		}
	});
}

$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-danger' id='SimpanTambahPelanggan'>Simpan Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$("#FormTambahPelanggan").find('input[type=text],textarea,select').filter(':visible:first').focus();

	$('#SimpanTambahPelanggan').click(function(e){
		e.preventDefault();
		TambahPelanggan();
	});

	$('#FormTambahPelanggan').submit(function(e){
		e.preventDefault();
		TambahPelanggan();
	});

	$('#tanggal_lahir').datetimepicker({
		lang:'en',
		timepicker:false,
		format:'Y-m-d',
		closeOnDateSelect:true
	});

	$( "#tps" ).select2({
   		dropdownParent: $("#ModalGue"),
		 placeholder: ' Select No TPS',
		ajax: {
			url: "<?php echo base_url('relawan/get_tps')?>",
			dataType: 'json',
			delay: 0,
			data: function (params) {
				return {
					q: params.term
				};
			},
			processResults: function (data) {
				// parse the results into the format expected by Select2.
				// since we are using custom formatting functions we do not need to
				// alter the remote JSON data
				return {
					results: data
				};
			},
			cache: false
		}
	});

	$( "#ref" ).select2({
   		dropdownParent: $("#ModalGue"),
		placeholder: ' Select Refrensi',
		allowClear: true,
		ajax: {
			url: "<?php echo base_url('relawan/get_ref')?>",
			dataType: 'json',
			delay: 0,
			data: function (params) {
				return {
					q: params.term
				};
			},
			processResults: function (data) {
				// parse the results into the format expected by Select2.
				// since we are using custom formatting functions we do not need to
				// alter the remote JSON data
				return {
					results: data
				};
			},
			cache: false
		}
	});
});
</script>
